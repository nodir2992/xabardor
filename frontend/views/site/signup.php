<?php

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */


$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-md-5 col-md-8">
            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?php
            /*echo $form->field($model, 'username')->textInput(['autofocus' => true])
                . $form->field($model, 'first_name')
                . $form->field($model, 'last_name')
                . $form->field($model, 'image')->widget(\kartik\file\FileInput::className(), [
                    'options' => ['accept' => 'image/*', 'id' => 'photo'],
                    'pluginOptions' => [
                        'showCaption' => false,
                        'showRemove' => false,
                        'showUpload' => false,
                        'maxFileSize' => 500,
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'allowedFileExtensions' => ['jpg', 'png']
                    ]
                ]);
            $this->registerCss(".file-preview { width: 0; display: table; }");*/
            ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>

            <?= $form->field($model, 'verify_code')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-md-5">{input}</div></div>{image}<button type="button" id="captcha_refresh" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i></button>',
                'imageOptions' => ['id' => 'captcha_image']
            ]) ?>
            <?= $this->registerJs("jQuery('#captcha_refresh').on('click', function(e){
                    e.preventDefault(); jQuery('#captcha_image').yiiCaptcha('refresh'); })") ?>

            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
