<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\imagine\Image;
use common\models\User;
use yii\web\UploadedFile;

/**
 *
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $image
 * @property string $info
 * @property string $password_repeat
 *
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $first_name;
    public $last_name;
    public $image;
    public $info;
    public $password;
    public $password_repeat;
    public $verify_code;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'trim'],
            [['username'], 'unique', 'targetClass' => '\common\models\User'],
//            [['username'], 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u'],
//            [['username', 'first_name', 'last_name'], 'required'],
            [['username', 'first_name', 'last_name'], 'string', 'min' => 2, 'max' => 32],

            [['email'], 'trim'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 64],
            [['email'], 'unique', 'targetClass' => '\common\models\User'],

            [['password', 'password_repeat'], 'required'],
            [['password'], 'string', 'min' => 6],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],

            [['image'], 'string', 'max' => 255],
            [['info'], 'string'],

            ['verify_code', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('model', 'Username'),
            'email' => Yii::t('model', 'Email'),
            'first_name' => Yii::t('model', 'First name'),
            'last_name' => Yii::t('model', 'Last name'),
            'image' => Yii::t('model', 'Image'),
            'info' => Yii::t('model', 'Info'),
            'password' => Yii::t('model', 'Password'),
            'password_repeat' => Yii::t('model', 'Password repeat'),
            'verify_code' => Yii::t('model', 'Verification Code'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->username = $this->email;

            /*if ($file = UploadedFile::getInstance($this, 'image')) {
                $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                $generateName = $this->username . '_' . time() . ".{$ext}";
                $path = Yii::getAlias('@uploadPath') . '/users/' . $generateName;
                if ($file->saveAs($path)) {
                    Image::thumbnail($path, 100, 100)
                        ->save(Yii::getAlias(Yii::getAlias('@uploadPath') . '/users/thumb/' . $generateName), ['quality' => 50]);
                    $this->image = Yii::getAlias('@uploadUrl') . '/users/thumb/' . $generateName;
                }
            }*/

            return true;
        } else {
            return false;
        }
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->image = $this->image;
        $user->info = $this->info;
        $user->status = User::STATUS_INACTIVE;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }

    /**
     * Sends an email with a link, for activate user.
     * @param User $user
     * @return bool whether the email was send
     */
    public function sendEmail($user)
    {
        try {
            return Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'confirmEmail-html', 'text' => 'confirmEmail-text'],
                    ['user' => $user]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                ->setTo($user->email)
                ->setSubject('Confirm email for ' . Yii::$app->name)
                ->send();
        } catch (\Swift_SwiftException $exception) {
            $user->delete();
            return false;
        }
    }
}
