<?php

namespace backend\controllers;

use yii\web\Controller;

class ContentController extends Controller
{

    /**
     * Displays FileManges page.
     * @return string
     */
    public function actionFiles()
    {
        return $this->render('files');
    }

}
