<?php

namespace backend\extensions\ckeditor;

use yii\web\AssetBundle;


class AssetsJQueryAdapter extends AssetBundle
{
    public $sourcePath = '@backend/extensions/ckeditor/editor/adapters';

    public $js = [
        'jquery.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'backend\extensions\ckeditor\Assets'
    ];
}
