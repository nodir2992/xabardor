<?php

namespace backend\extensions\ckeditor;

use yii\web\AssetBundle;

class Assets extends AssetBundle
{
    public $sourcePath = '@backend/extensions/ckeditor/editor';

    public $js = [
        'ckeditor.js',
        'js.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
